
okteto 注册地址： https://cloud.okteto.com

本专案用于在 Okteto上部署 V2Ray WebSocket，
部署完成后，每次启动应用时，运行的 V2Ray 将始终为最新版本

部署: 
可fork本专案到自己的账户, 需将entrypoint.sh文件中的ID修改为自己的UUID (见下面的变量说明)
UUID可以通过在线生成工具来自己生成 https://1024tools.com/uuid
最后进入自己的okteto空间 https://cloud.okteto.com/ 
复制自己gitlab项目的地址, 点击deploy进行部署

### 变量

对部署时需设定的变量名称做如下说明。

| 变量 | 默认值 | 说明 |
| :--- | :--- | :--- |
| `ID` | `ad806487-2d26-4636-98b6-ab85cc8521f7` | VMess 用户主 ID，用于身份验证，为 UUID 格式 |
| `AID` | `64` | 为进一步防止被探测所设额外 ID，即 AlterID，范围为 0 至 65535 |
| `WSPATH` | `/` | WebSocket 所使用的 HTTP 协议路径 |


详情见： https://github.com/yunbaitech666/okvtwo
